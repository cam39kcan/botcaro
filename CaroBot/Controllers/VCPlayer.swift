//
//  VCPlayer.swift
//  CaroBot
//
//  Created by Đỗ Hồng Quân on 5/11/19.
//  Copyright © 2019 Đỗ Hồng Quân. All rights reserved.
//

import UIKit

struct Move {
    var row: Int
    var col: Int
}

class VCPlayer: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionViewOfPlay: UICollectionView!
    @IBOutlet weak var lblPlayer: UILabel!
    @IBOutlet weak var lblBot: UILabel!
    
    
    // MARK: - Variables
    let numberOfItem = 9
    var stateOfItem: [[Bool?]] = [[Bool?]]()
    var getSqrt: Int!
    var player = Player()
    var bot = Bot()
    static let winCondition = 3
    static var playerWin = 0
    static var botWin = 0
    var arr: [Bool?] = [Bool?]()
    
    // MARK: - Closures

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
        initState()
    }
    
    // MARK: - Setup
    private func setupCollection() {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let itemRow = sqrt(Double(numberOfItem))
        let itemWidth = (view.bounds.width - ((CGFloat(itemRow) - 1) * layout.minimumInteritemSpacing) - layout.sectionInset.left - layout.sectionInset.right) / CGFloat(itemRow)
        let itemHeight = itemWidth
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        collectionViewOfPlay.collectionViewLayout = layout
        collectionViewOfPlay.dataSource = self
        collectionViewOfPlay.delegate = self
        collectionViewOfPlay.register(UINib(nibName: "CellRow", bundle: nil), forCellWithReuseIdentifier: "CellRow")
        collectionViewOfPlay.reloadData()
        
    }
    
    private func initState() {
        getSqrt = Int(sqrt(Double(numberOfItem)))
        if let getSqrt = getSqrt {
            for _ in 0..<getSqrt {
                var tempState: [Bool?] = [Bool?]()
                for _ in 0..<getSqrt {
                    tempState.append(nil)
                }
                stateOfItem.append(tempState)
            }
        }
        
        for i in 0..<numberOfItem {
            arr.append(nil)
        }
    }
    
    private func removeAllElement() {
        stateOfItem.removeAll()
    }
    
    // MARK: - Actions
    @IBAction func replayGame(_ sender: Any) {
        collectionViewOfPlay.allowsSelection = true
        removeAllElement()
        initState()
        player = Player()
        bot = Bot()
        collectionViewOfPlay.reloadData()
    }
    
    // MARK: - Supporting Methods
    private func isFullOfMove(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            for j in 0..<state[i].count {
                if state[i][j] == nil {
                    return false
                }
            }
        }
        return true
    }
    
    private func heuristicForWin(_ state: [[Bool?]]) -> Int {
        if player.checkWin(state) {
            return -1000
        } else if bot.checkWinBot(state) {
            return 1000
        }
        return 0
    }
    
    private func minimax(_ state: inout [[Bool?]], _ depthOfTree: Int?, _ isMaximizer: Bool?) -> Int {
        let score = heuristicForWin(state)
    
        if score > 0 {
            return score - depthOfTree!
        }
        
        if score < 0 {
            return score + depthOfTree!
        }
        
        if isFullOfMove(state) {
            return 0
        }
        
        if isMaximizer! {
            var highest = -1000
            for i in 0..<state.count {
                for j in 0..<state[i].count {
                    if state[i][j] == nil {
                        state[i][j] = false
                        highest = max(highest, minimax(&state, depthOfTree! + 1, !isMaximizer!))
                        state[i][j] = nil
                    }
                }
            }
            return highest
        } else {
            var lowest = 1000
            for i in 0..<state.count {
                for j in 0..<state[i].count {
                    if state[i][j] == nil {
                        state[i][j] = true
                        lowest = min(lowest, minimax(&state, depthOfTree! + 1, !isMaximizer!))
                        state[i][j] = nil
                    }
                }
            }
            return lowest
        }
    }
    
    private func findOptimalMove(_ state: inout [[Bool?]]) -> Move {
        var bestVal = -1000
        var bestMove = Move(row: -1, col: -1)
        
        for i in 0..<state.count {
            for j in 0..<state[i].count {
                if state[i][j] == nil {
                    state[i][j] = false
                    let tempVal = minimax(&state, 0, false)
                    state[i][j] = nil
                    
                    if tempVal > bestVal {
                        bestVal = tempVal
                        bestMove.row = i
                        bestMove.col = j
                    }
                }
            }
        }
        return bestMove
    }
    
}

extension VCPlayer: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItem
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let item = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRow", for: indexPath) as? CellRow else {
            return UICollectionViewCell()
        }
        
        if let unselectableItem = stateOfItem[indexPath.row / getSqrt][indexPath.row % getSqrt] {
            if unselectableItem {
                item.setIconForCell(UIImage(named: "X_icon"))
            } else {
                item.setIconForCell(UIImage(named: "O_icon"))
            }
        } else {
            item.setIconForCell(nil)
        }
        return item
    }
}

extension VCPlayer: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if stateOfItem[indexPath.row / getSqrt][indexPath.row % getSqrt] == nil {
            stateOfItem[indexPath.row / getSqrt][indexPath.row % getSqrt] = true
            if !isFullOfMove(stateOfItem) {
                let botMove = findOptimalMove(&stateOfItem)
                stateOfItem[botMove.row][botMove.col] = false
            }
            collectionView.reloadData()
            if player.checkWin(stateOfItem) {
                VCPlayer.playerWin += 1
                lblPlayer.text = "Người thắng: \(VCPlayer.playerWin)"
                let alert = UIAlertController(title: "", message: "Người thắng", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel) { (act) in
                    collectionView.allowsSelection = false
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            } else if bot.checkWinBot(stateOfItem) {
                VCPlayer.botWin += 1
                lblBot.text = "Máy thắng: \(VCPlayer.botWin)"
                let alert = UIAlertController(title: "", message: "Máy thắng", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel) { (act) in
                    collectionView.allowsSelection = false
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            } else if isFullOfMove(stateOfItem) {
                let alert = UIAlertController(title: "", message: "Hòa", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .cancel) { (act) in
                    collectionView.allowsSelection = false
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
