//
//  CellRow.swift
//  CaroBot
//
//  Created by Đỗ Hồng Quân on 5/11/19.
//  Copyright © 2019 Đỗ Hồng Quân. All rights reserved.
//

import UIKit

class CellRow: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var iconForCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    public func setIconForCell(_ img: UIImage?) {
        self.iconForCell.image = img
    }
    
}
