//
//  Bot.swift
//  CaroBot
//
//  Created by Đỗ Hồng Quân on 5/11/19.
//  Copyright © 2019 Đỗ Hồng Quân. All rights reserved.
//

import Foundation

class Bot {
    
    init() {
        
    }
    
    public func checkWinBot(_ state: [[Bool?]]) -> Bool {
        if horizontalWinBot(state) || verticalWinBot(state) || diagonalLTRWinBot(state) || diagonalRTLWinBot(state) {
            return true
        }
        return false
    }
    
    private func horizontalWinBot(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            var counter = 1
            for j in 0..<state[i].count - 1 {
                if let nowState = state[i][j], let nextState = state[i][j+1] {
                    if nowState == nextState && !nowState {
                        counter += 1
                        if counter == VCPlayer.winCondition {
                            return true
                        }
                    } else {
                        counter = 1
                    }
                }
            }
        }
        return false
    }
    
    private func verticalWinBot(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            var counter = 1
            for j in 0..<state[i].count - 1 {
                if let nowState = state[j][i], let nextState = state[j+1][i] {
                    if nowState == nextState && !nowState {
                        counter += 1
                        if counter == VCPlayer.winCondition {
                            return true
                        }
                    } else {
                        counter = 1
                    }
                }
            }
        }
        return false
    }
    
    private func diagonalLTRWinBot(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            for j in 0..<state[i].count {
                var x = i; var y = j
                var counter = 1
                while x < state.count - 1 && y < state[i].count - 1 {
                    if let nowState = state[x][y], let nextState = state[x+1][y+1] {
                        if nowState == nextState && !nowState {
                            counter += 1
                            if counter == VCPlayer.winCondition {
                                return true
                            }
                        } else {
                            counter = 1
                        }
                    }
                    x += 1; y += 1
                }
            }
        }
        return false
    }
    
    private func diagonalRTLWinBot(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            for j in stride(from: state[i].count - 1, through: 1, by: -1) {
                var x = i; var y = j
                var counter = 1
                while x < state.count - 1 && y > 0 {
                    if let nowState = state[x][y], let nextState = state[x+1][y-1] {
                        if nowState == nextState && !nowState {
                            counter += 1
                            if counter == VCPlayer.winCondition {
                                return true
                            }
                        } else {
                            counter = 1
                        }
                    }
                    x += 1; y -= 1
                }
            }
        }
        return false
    }
    
}
