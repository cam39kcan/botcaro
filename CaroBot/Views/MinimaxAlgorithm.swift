//
//  MinimaxAlgorithm.swift
//  CaroBot
//
//  Created by Đỗ Hồng Quân on 5/12/19.
//  Copyright © 2019 Đỗ Hồng Quân. All rights reserved.
//

import Foundation


class MinimaxAlgorithm {
    
    private var player: Player!
    private var bot: Bot!
    
    init(_ player: Player!, _ bot: Bot!) {
        self.player = player
        self.bot = bot
    }
    
    
    public func isFullOfMove(_ state: [[Bool?]]) -> Bool {
        for i in 0..<state.count {
            for j in 0..<state[i].count {
                if state[i][j] == nil {
                    return false
                }
            }
        }
        return true
    }
    
    public func heuristicForWin(_ state: [[Bool?]]) -> Int {
        if self.player.checkWin(state) {
            return -1000
        } else if self.bot.checkWinBot(state) {
            return 1000
        }
        return 0
    }
    
    public func minimax(_ state: inout [[Bool?]], _ depthOfTree: Int?, _ isMaximizer: Bool?) -> Int {
        let score = heuristicForWin(state)
        
        if score > 0 {
            return score - depthOfTree!
        }
        
        if score < 0 {
            return score + depthOfTree!
        }
        
        if isFullOfMove(state) {
            return 0
        }
        
        if isMaximizer! {
            var highest = -1000
            for i in 0..<state.count {
                for j in 0..<state[i].count {
                    if state[i][j] == nil {
                        state[i][j] = false
                        highest = max(highest, minimax(&state, depthOfTree! + 1, !isMaximizer!))
                        state[i][j] = nil
                    }
                }
            }
            return highest
        } else {
            var lowest = 1000
            for i in 0..<state.count {
                for j in 0..<state[i].count {
                    if state[i][j] == nil {
                        state[i][j] = true
                        lowest = min(lowest, minimax(&state, depthOfTree! + 1, !isMaximizer!))
                        state[i][j] = nil
                    }
                }
            }
            return lowest
        }
    }
    
    public func findOptimalMove(_ state: inout [[Bool?]]) -> Move {
        var bestVal = -1000
        var bestMove = Move(row: -1, col: -1)
        
        for i in 0..<state.count {
            for j in 0..<state[i].count {
                if state[i][j] == nil {
                    state[i][j] = false
                    let tempVal = minimax(&state, 0, false)
                    state[i][j] = nil
                    
                    if tempVal > bestVal {
                        bestVal = tempVal
                        bestMove.row = i
                        bestMove.col = j
                    }
                }
            }
        }
        return bestMove
    }
}
